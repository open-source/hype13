Ce dépôt contient un script python qui va prendre plusieurs sources d'informations au format CSV pour générer un fichier au format lheo 2.3 http://lheo.gouv.fr/2.3/ .
Le premier csv peut provenir d'apogée, le second d'un outil qui décrit de façon plus fine les formations.

L'outil pour produire une offre de formation au format Lheo 2.3 est dans la branche hype_2.3 : https://git.unicaen.fr/open-source/hype13/-/tree/hype_2.3
